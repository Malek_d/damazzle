<?php
namespace App\Traits;

use App\Helpers\ResponseCode;
use Illuminate\Http\JsonResponse;
trait Response
{
    protected function successWithoutData(string $message): JsonResponse
    {
        return response()->json([
            "success" => true,
            "code" => ResponseCode::SUCCESS,
            "message" => $message,
            "server_time" => date('Y-m-d H:i:s')
        ]);
    }

    protected function successWithData(string $message, $data): JsonResponse
    {
        return response()->json([
            "success" => true,
            "code" => ResponseCode::SUCCESS,
            "data" => $data,
            "message" => $message,
            "server_time" => date('Y-m-d H:i:s')
        ]);
    }

    protected function errorResponse(string $message, $code = ResponseCode::GENERAL_ERROR): JsonResponse
    {
        return response()->json([
            "success" => false,
            "code" => $code,
            "message" => $message,
            "server_time" => date('Y-m-d H:i:s')
        ], 422);
    }

    protected function unauthenticatedResponse(): JsonResponse
    {
        return response()->json([
            "success" => false,
            "code" => ResponseCode::UN_AUTHENTICATED,
            "message" => "Unauthenticated",
            "server_time" => date('Y-m-d H:i:s')
        ], 403);
    }

    protected function generalError(): JsonResponse
    {
        return response()->json([
            "success" => false,
            "code" => ResponseCode::GENERAL_ERROR,
            "message" => "General Error",
            "server_time" => date('Y-m-d H:i:s')
        ], 422);
    }

}