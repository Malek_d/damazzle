<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCarRequest extends FormRequest
{
    public function rules()
    {
        return [
            'title' => [
                'string',
                'nullable',
            ],
            'initial_price' => [
                'nullable',
            ],
            'is_sold'=>[
                'nullable'
            ]
        ];
    }

}