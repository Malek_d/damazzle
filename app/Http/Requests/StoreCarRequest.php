<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCarRequest extends FormRequest
{
    public function rules()
    {
        return [
            'title' => [
                'string',
                'required',
            ],
            'initial_price' => [
                'required',
            ],
            'is_sold'=>[
                'nullable'
            ]
        ];
    }
}