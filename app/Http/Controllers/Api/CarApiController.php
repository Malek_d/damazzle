<?php

namespace App\Http\Controllers\Api;

use \App\Http\Controllers\Controller;
use App\Http\Requests\StoreCarRequest;
use App\Http\Requests\UpdateCarRequest;
use App\Http\Resources\CarResource;
use App\Models\Car;
use App\Traits\Response;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CarApiController extends Controller
{
    use Response;

    public function index(): \Illuminate\Http\JsonResponse
    {
        $user = auth()->user();

        $cars = $user->Cars;

        return $this->successWithData("All Cars", [
            "cars" => (new CarResource($cars)),
        ]);
    }

    public function store(StoreCarRequest $storeCarRequest): \Illuminate\Http\JsonResponse
    {
        $requestData = $storeCarRequest->all();

        $requestData['user_id'] = auth()->user()->id;

        $car = Car::create($requestData);

        return $this->successWithData("Car added successfully", [
            "car" => (new CarResource($car)),
        ]);
    }

    public function show($id): \Illuminate\Http\JsonResponse
    {
        $user = auth()->user();
        $car = $user->Cars()->where('id', $id)->first();
        if ($car)
            return $this->successWithData("All Cars", [
                "car" => (new CarResource($car)),
            ]);
        else
            throw new NotFoundHttpException("Car not found");
    }

    public function update(UpdateCarRequest $updateCarRequest,$id): \Illuminate\Http\JsonResponse
    {
        $user = auth()->user();

        $car = $user->Cars()->where('id', $id)->first();

        if (!$car)
            throw new NotFoundHttpException("Car not found");

        $car->update($updateCarRequest->all());

        return $this->successWithData("Car updated successfully", [
            "car" => (new CarResource($car)),
        ]);
    }

    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        $user = auth()->user();

        $car = $user->Cars()->where('id', $id)->first();

        if (!$car)
            throw new NotFoundHttpException("Car not found");

        $car->delete();

        return $this->successWithoutData("Car deleted successfully");
    }
    public function search(Request $request): \Illuminate\Http\JsonResponse
    {

        $requestData = $request->validate([
            'term' => 'required|string|max:100'
        ]);
        $user = auth()->user();

        $cars = Car::where('title', 'LIKE', '%' . $requestData['term'] . '%')
            ->where('is_sold', 0)
            ->where('user_id','!=',$user->id)
            ->get();


        return $this->successWithData("All Cars", [
            "cars" => (new CarResource($cars)),
        ]);
    }
    public function searchWithUserCars(Request $request): \Illuminate\Http\JsonResponse
    {

        $requestData = $request->validate([
            'term' => 'required|string|max:100'
        ]);
        $user = auth()->user();

        $cars = Car::where('title', 'LIKE', '%' . $requestData['term'] . '%')
            ->where('is_sold', 0)
            ->get();


        return $this->successWithData("All Cars", [
            "cars" => (new CarResource($cars)),
        ]);
    }

}