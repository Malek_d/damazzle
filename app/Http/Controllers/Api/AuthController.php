<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseCode;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    use Response;
    public function register(Request $request)
    {
        $requestData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:6|confirmed'
        ]);

        $user = User::create([
            'name' => $requestData['name'],
            'password' => bcrypt($requestData['password']),
            'email' => $requestData['email']
        ]);

        return $this->successWithData("Register successfully",[
            'token' => $user->createToken('API_Token')->plainTextToken
        ]);

    }

    public function login(Request $request){
        $requestData = $request->validate([
            'email' => 'required|string|email|',
            'password' => 'required|string|min:6'
        ]);

        if (!Auth::attempt($requestData)) {
            return $this->errorResponse('Credentials not match', ResponseCode::UN_AUTHENTICATED);
        }
        //Revoke all token
        auth()->user()->tokens()->delete();
        return $this->successWithData("Register successfully",[
            'token' => auth()->user()->createToken('API_Token')->plainTextToken
        ]);
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();

        return $this->successWithoutData("Tokens Revoked");
    }
}