<?php

namespace App\Exceptions;

use App\Helpers\ResponseCode;
use App\Traits\Response;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use Response;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e){

        if ($e instanceof MethodNotAllowedHttpException) {
            return $this->errorResponse('Not Allowed', ResponseCode::NOT_ALLOWED);
        }

        if ($e instanceof NotFoundHttpException) {
            return $this->errorResponse('Not Found', ResponseCode::NOT_FOUND);
        }
        return parent::render($request, $e);
    }
}
