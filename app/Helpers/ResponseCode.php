<?php

namespace App\Helpers;

class ResponseCode
{
    const SUCCESS = 'S001';

    const NOT_FOUND = "E001";

    const NOT_ALLOWED = "E002";

    const UN_AUTHENTICATED = "E003";

    const GENERAL_ERROR = "E004";
}